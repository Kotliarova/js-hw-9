// Опишіть, як можна створити новий HTML тег на сторінці.
// це можна зробити за допомогою document.createElement() - в () пишеться тег в кавичках. І щоб додати його на сторінку треба використати наприклад append()

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметер це позиція, він може мати такі значення: beforebegin (перед елементом), afterbegin (одразу після відкриваючого тега елементу тобто як перший child), beforeend (перед тегом закриття елеметна тобто як останій child) та afterend (після елемента).

// Як можна видалити елемент зі сторінки?
// за доромогою remove() або removeChild()


const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function arrayList (arr, parent = document.body) {
    const list = document.createElement('ul');
    arr.forEach(elem => {
        const li = document.createElement('li');
        if (!Array.isArray(elem)) {
            li.innerText = elem;
        } else {
            arrayList (elem, li);
        }
        list.append(li);
    });
    parent.append(list);
}
arrayList(arr3);

const div = document.createElement('div');
div.style.fontSize = '20px';
div.style.marginTop = '20px';
div.style.textAlign = 'center';
document.body.append(div);
let interval = 3;
div.innerText = `Time left: ${interval} sec`;

function timer() {
    interval--;
    div.innerText = `Time left: ${interval} sec`;
    if (interval <= 0) {
        document.body.remove();
        clearInterval(sec)
    }
}
const sec = setInterval(timer, 1000)
